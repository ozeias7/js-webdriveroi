import Base from '../Base'

class FeedbackPage extends Base {
   get form (){
       return $('form')
   }

   get name (){
       return $('#name')
   }

   get email (){
       return $ ('#email')
   }

   get subjet (){
       return $ ('#subject')
   }

   get message (){
       return $ ('#comment')
   }

   get submitButton () {
       return $ ('input[type="submit"]')
   }



   // Métodos

   formIsVisible (){
       this.form.waitForExist()
   }

   fillForme(name, email, subjet, message){
       this.name.setValue(name)
       this.email.setValue(email)
       this.subjet.setValue(subjet)
       this.message.setValue(message)
   }

   submitForm(){
       this.submitButton.click()
   }

} 


export default new FeedbackPage()