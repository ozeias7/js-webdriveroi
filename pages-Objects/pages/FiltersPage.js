import Base from '../Base'

class FiltersPage extends Base {

    get descriptionInput(){
        return $('#aa_description')
    }

    get submitButton(){
        return $('button[type="submit"]')
    }


    get resultTable(){
        return $('#filtered_transactions_for_account')
    }

    get message(){
        return $('.well')
    }
    

    // Métodos

    fillDescription (){
        this.descriptionInput.waitForExist()
        this.descriptionInput.setValue('Test')
    }

    submitFilter(){
        this.submitButton.click()
    }

    resultsTableIsVisible(){
        this.resultTable.waitForExist()
        this.resultTable.click()
    }



}

export default new FiltersPage()