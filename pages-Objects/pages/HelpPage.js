import Base from '../Base'

    class HelpPage extends Base {

        get title (){
            return $('.span8 > h3')
        }

        get transferFundsLink(){
            return $('*=transfer funds')
        }

        get paybillsLink(){
            return $('*=pay bills')
        }


        // Métodos

        clickTransferenciafunds(){
            this.transferFundsLink.waitForExist()
            this.transferFundsLink.click()
        }

        clickpayBillsLink(){
            this.paybillsLink.waitForExist()
            this.paybillsLink.click()
        }

    }
    export default new HelpPage()