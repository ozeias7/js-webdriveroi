class Base {
  
    pauseShort() {
        browser.pause(2000)
    }

    pauseMedium() {
        browser.pause(3000)
    }

    pauseLong() {
        browser.pause(4000)
    }
}

export default Base