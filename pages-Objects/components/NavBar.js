import Base from '../Base'

class NavBar extends Base {

    get searcBox(){
        return $('#searchTerm')
    }

    get signInButton(){
        return $('#signin_button')
    }

    get insidnavBar(){  
        return $('.nav-tabs')
    }

    get settingsButton(){
        return $('.icon-cog')
    }

    get helpButton (){
        return $('#help_link')
    }

    // Métodos


    serrch(text) {
        this.searcBox.waitForExist()
        this.searcBox.setValue(text)
        browser.keys('Enter')
    }


   signInButtonInvisible(){
       this.signInButton.waitFExist()
   }

   clickSignIn(){
       this.signInButton.waitForExist()
       this.signInButton.click()
   }

   insidNavBarIsVisible() {
       this.insidnavBar.waitForExist()
   }

   clickSettings(){
       this.settingsButton.waitForExist()
       this.settingsButton.click()
   } 

   clickHelp(){
       this.helpButton.waitForExist()
       this.helpButton.click()
   }



}

export default new NavBar()