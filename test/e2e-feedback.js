
import App from '../pages-Objects/App'
import FeedbackPage from '../pages-Objects/pages/FeedbackPage'

describe('E2E - Feedback', () => {
	it('Should load feedback form', () => {
		App.openFeedbackPage()
		//$('form').waitForExist()
		FeedbackPage.formIsVisible()
	})
 
	it('Should submit feedback form', () => {
		//$('#name').setValue('testes')
		//$('#email').setValue('test@test.com')
		//$('#subject').setValue('Subjects')
		//$('#comment').setValue('Just a message!')
		//$('input[type="submit"]').click()
		FeedbackPage.fillForme('name', 'test@test.com', 'Subjects', 'message')
		FeedbackPage.submitForm()
		expect(browser).toHaveUrl(
			'http://zero.webappsecurity.com/sendFeedback.html'
		)
	})
})