import App from '../pages-Objects/App'
import LoginPage from '../pages-Objects/pages/LoginPage'
import NavBar from '../pages-Objects/components/NavBar'


describe('E2E Tests - Login / Logout Flow', () => {
    it('Should not login with invalid credentials', () => {
        App.openHomePage()
        NavBar.clickSignIn()
        LoginPage.login('username','password')
        const message = LoginPage.error
        expect(message).toHaveText('Login and/or password are wrong.')
    })

    it('Should login with valid credentials', () => {
        App.openHomePage()
        NavBar.clickSignIn()
        LoginPage.formIsVisible()
        LoginPage.fillForma('username', 'password')
        LoginPage.submitForm()
        NavBar.insidNavBarIsVisible()
 
    })
 
    it('Should logout from app', () => {
        App.logout()
        NavBar.signInButtonInvisible()


    })

})