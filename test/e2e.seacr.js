
import App from '../pages-Objects/App'
import NavBar from '../pages-Objects/components/NavBar'

describe('E2E Tests - Search', () => {
	it('Should load homepage', () => {
		App.openHomePage()
	})
 
	it('Should submit searchbox', () => {	
		NavBar.serrch('bank')
		const resultsTitle = $('h2')
		resultsTitle.waitForExist()
		expect(resultsTitle).toHaveText('Search Results:')
	})
})