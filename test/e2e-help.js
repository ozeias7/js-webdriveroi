import App from '../pages-Objects/App'
import LoginPage from '../pages-Objects/pages/LoginPage'
import HelPage from '../pages-Objects/pages/HelpPage'
import NavBar from '../pages-Objects/components/NavBar'



describe('E2E Testing - Help Selection', () => {
    it('Should log into application', () => {
      App.openLoginPage()
      LoginPage.login('username','password')
      NavBar.insidNavBarIsVisible()
    })
  
    it('Shold load help Content', () => {
        NavBar.clickSettings()
      
        NavBar.clickHelp()
        const title = HelPage.title
        expect(title).toHaveText('How do I log into my account?')
        HelPage.clickTransferenciafunds()
        expect(title).toHaveText('How do I transfer funds?')
        HelPage.clickpayBillsLink
        expect(title).toHaveText('How do I pay bills?')
    })
 
})