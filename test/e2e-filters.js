import App from '../pages-Objects/App'
import LoginPage from '../pages-Objects/pages/LoginPage'
import FiltersPage from '../pages-Objects/pages/FiltersPage'
import NavBar from '../pages-Objects/components/NavBar'
import InsideNavbar from '../pages-Objects/components/InsideNavbar'

describe('E2E Tests - Transaction', () => {
    it('Should log into application', () => {
	App.openLoginPage()
    LoginPage.login('username','password')
	NavBar.insidNavBarIsVisible()
        
    })
    
    it('Transaction Filler Should work', () => {
        InsideNavbar.clickaccountActivityTab()
        InsideNavbar.clickfiltersLink()
       FiltersPage.fillDescription('Test')
       FiltersPage.submitFilter()
       FiltersPage.resultsTableIsVisible()
       const message = FiltersPage.message
       expect(message).toHaveText('No results.')

    })


    
    
})